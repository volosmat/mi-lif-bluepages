<%@include file="../init.jspf" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="static configuration.EmployeeListPortletConstants.*" %>

<portlet:defineObjects />

<c:if test="${successMessage != null}">
	<div class="alert alert-success">
		${successMessage}
	</div>
</c:if>

<c:if test="${errorMessage != null}">
	<div class="alert alert-danger">
			${errorMessage}
	</div>
</c:if>

<p><button class="btn btn-success" type="button" id="add_employee_button">Add employee</button></p>

<div id="add_employee_form" style="display: none">
	<portlet:actionURL var="actionUrl" name="<%=SAVE_EMPLOYEE_ACTION%>"/>
	<form:form action="${actionUrl}" method="POST" modelAttribute="<%=FORM_MODEL%>">
		<form:hidden path="id"/>
		<table width="100%">
			<tr>
				<td>
					<form:label path="firstName">First name</form:label>
					<form:input path="firstName" cssStyle="width: 95%;"/>
					<form:errors path="firstName" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
				<td>
					<form:label path="address.street">Street</form:label>
					<form:input path="address.street" cssStyle="width: 95%;"/>
					<form:errors path="address.street" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
			</tr>
			<tr>
				<td>
					<form:label path="surname">Surname</form:label>
					<form:input path="surname" cssStyle="width: 95%;"/>
					<form:errors path="surname" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
				<td>
					<form:label path="address.houseNumber">House number</form:label>
					<form:input path="address.houseNumber" cssStyle="width: 95%;"/>
					<form:errors path="address.houseNumber" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
			</tr>
			<tr>
				<td>
					<form:label path="email">Email address</form:label>
					<form:input path="email" cssStyle="width: 95%;"/>
					<form:errors path="email" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
				<td>
					<form:label path="address.city">City</form:label>
					<form:input path="address.city" cssStyle="width: 95%;"/>
					<form:errors path="address.city" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
			</tr>
			<tr>
				<td>
					<form:label path="workplace.positionName">Position name</form:label>
					<form:input path="workplace.positionName" cssStyle="width: 95%;"/>
					<form:errors path="workplace.positionName" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
				<td>
					<form:label path="address.state">State</form:label>
					<form:input path="address.state" cssStyle="width: 95%;"/>
					<form:errors path="address.state" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
			</tr>
			<tr>
				<td>
					<form:label path="workplace.description">Position description</form:label>
					<form:textarea path="workplace.description" cssStyle="width: 95%;"/>
					<form:errors path="workplace.description" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
				<td>
					<form:label path="address.zip">ZIP</form:label>
					<form:input path="address.zip" cssStyle="width: 95%;"/>
					<form:errors path="address.zip" cssStyle="width: 88%;" cssClass="alert alert-danger"/>
				</td>
			</tr>
		</table>

		<input type="submit" class="btn btn-default" value="Submit" />
	</form:form>
</div>


<div class="dataTable_wrapper">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Street</th>
				<th>City</th>
				<th>State</th>
				<th>Workplace</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="employee" items="${employeeList}" varStatus="iterator">

				<c:choose>
					<c:when test="${iterator.index % 2 == 0}">
						<c:set var="row_col" value="odd"/>
					</c:when>
					<c:otherwise>
						<c:set var="row_col" value="even"/>
					</c:otherwise>
				</c:choose>

				<tr class="${row_col} gradeX">
					<td>${employee.firstName} ${employee.surname}</td>
					<td>${employee.email}</td>
					<td>${employee.address.street} ${employee.address.houseNumber}</td>
					<td>${employee.address.city}</td>
					<td>${employee.address.state}</td>
					<td>${employee.workplace.positionName}</td>
					<td align="center" width="130px">
						<portlet:actionURL var="deleteURL" name="<%=DELETE_EMPLOYEE_ACTION%>">
							<portlet:param name="<%=PARAMETER_EMPLOYEE_ID%>" value="${employee.id}"/>
						</portlet:actionURL>
						<portlet:actionURL var="editURL" name="<%=EDIT_EMPLOYEE_ACTION%>">
							<portlet:param name="<%=PARAMETER_EMPLOYEE_ID%>" value="${employee.id}"/>
						</portlet:actionURL>
						<a href="${editURL}" class="btn btn-primary" type="button">Edit</a>
						<a href="${deleteURL}" class="btn btn-danger" type="button">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script>
	var delay = 500;
	var employeeId = ${employee.id};
	var hasErrors = false;

	<spring:hasBindErrors name="<%=FORM_MODEL%>">
		hasErrors = true;
	</spring:hasBindErrors>

	if (employeeId > 0 || hasErrors) {
		$("#add_employee_form").show();
	}
	$("#add_employee_button").click(function(){
		var formSelector = $("#add_employee_form");
		if (formSelector.is(':visible')) {
			formSelector.hide(delay);
		} else {
			formSelector.show(delay);
		}
	});
</script>