<%@include file="../init.jspf" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="static configuration.SearchPortletConstants.*" %>
<%@ page import="static configuration.EmployeeListPortletConstants.*" %>

<portlet:defineObjects />

<div>
  <portlet:actionURL var="searchURL" name="<%=SEARCH_ACTION %>"/>
  <form action="${searchURL}" method="post">
	<input type="text" name="<%=QUERY_STRING_PARAMETER %>" class="span10" autocomplete="off" placeholder="Search employee by name ...">
	<input type="submit" class="btn btn-primary span2" value="Search">
  </form>
</div>

<p>Found results: ${searchResult.size()}</p>

<c:if test="${searchResult.size() > 0}">
	<div class="dataTable_wrapper">
		<table class="table table-striped table-bordered table-hover" id="dataTables-example">
			<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<c:forEach var="employee" items="${searchResult}" varStatus="iterator">

				<c:choose>
					<c:when test="${iterator.index % 2 == 0}">
						<c:set var="row_col" value="odd"/>
					</c:when>
					<c:otherwise>
						<c:set var="row_col" value="even"/>
					</c:otherwise>
				</c:choose>

				<tr class="${row_col} gradeX">
					<td>${employee.firstName} ${employee.surname}</td>
					<td>${employee.email}</td>
					<td align="center" width="130px">
						<portlet:actionURL var="deleteURL" name="<%=DELETE_EMPLOYEE_ACTION%>">
							<portlet:param name="<%=PARAMETER_EMPLOYEE_ID%>" value="${employee.id}"/>
						</portlet:actionURL>
						<portlet:actionURL var="editURL" name="<%=EDIT_EMPLOYEE_ACTION%>">
							<portlet:param name="<%=PARAMETER_EMPLOYEE_ID%>" value="${employee.id}"/>
						</portlet:actionURL>
						<a href="${editURL}" class="btn btn-primary" type="button">Edit</a>
						<a href="${deleteURL}" class="btn btn-danger" type="button">Delete</a>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
</c:if>