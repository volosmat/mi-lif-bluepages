package configuration;

public class EmployeeListPortletConstants {

	public static final String SAVE_EMPLOYEE_ACTION = "saveEmployeeAction";
	public static final String DELETE_EMPLOYEE_ACTION = "deleteEmployeeAction";
	public static final String PARAMETER_EMPLOYEE_ID = "employeeID";
	public static final String EDIT_EMPLOYEE_ACTION = "editEmployeeAction";
	public static final String FORM_MODEL = "employee";
	public static final String EMPLOYEE_LIST = "employeeList";
	public static final String JSP_DEFAULT = "list/employee-list";
}
