package configuration;

public class SearchPortletConstants {

	public static final String SEARCH_ACTION = "search";
	public static final String JSP_DEFAULT = "search/view";
	public static final String SEARCH_RESULT_LIST = "searchResult";
	public static final String QUERY_STRING_PARAMETER = "queryString";
}
