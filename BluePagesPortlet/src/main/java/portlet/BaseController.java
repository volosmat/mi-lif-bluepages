package portlet;

import bluepages.backend.entity.Employee;
import bluepages.backend.service.EmployeeService;
import bluepages.backend.service.ServiceFactory;
import org.springframework.ui.Model;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

import javax.persistence.EntityNotFoundException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import static configuration.EmployeeListPortletConstants.*;

public class BaseController {

	@ActionMapping(DELETE_EMPLOYEE_ACTION)
	public void deleteEmployeeAction(ActionRequest request, ActionResponse response,  Model model) {
		// Load employee
		Employee employee = this.loadEmployee(request.getParameter(PARAMETER_EMPLOYEE_ID), model);
		// Remove employee
		if (employee != null) {
			ServiceFactory.getEmployeeService().removeEmployee(employee);
			model.addAttribute("successMessage", "Employee record was deleted successfully.");
		}
	}

	@ActionMapping(EDIT_EMPLOYEE_ACTION)
	public void editEmployeeAction(ActionRequest request, ActionResponse response) {
		// Pass employee id parameter to render method
		response.setRenderParameter(PARAMETER_EMPLOYEE_ID, request.getParameter(PARAMETER_EMPLOYEE_ID));
	}

	/**
	 * Load employee record from db or show error message if idParameter is incorrect or employee not exists
	 * @param idParameter
	 * @param model
	 * @return
	 */
	protected Employee loadEmployee(String idParameter, Model model) {
		try {
			EmployeeService employeeService = ServiceFactory.getEmployeeService();
			int employeeId = Integer.parseInt(idParameter);

			return employeeService.getEmployee(employeeId);
		} catch (EntityNotFoundException ignored) {
		} catch (NumberFormatException e) {
			model.addAttribute("errorMessage", "Incorrect employee id.");
		}

		return null;
	}
}
