package portlet.list;

import bluepages.backend.entity.Employee;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class EmployeeValidator implements Validator {

	/**
	 * This Validator validates *just* Employee instances
	 */
	public boolean supports(Class clazz) {
		return Employee.class.equals(clazz);
	}

	public void validate(Object obj, Errors errors) {
		Employee employee = (Employee) obj;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "error-message-required-first-name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "error-message-required-surname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "error-message-required-email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.state", "error-message-required-state");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.houseNumber", "error-message-required-house-number");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.street", "error-message-required-street");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.city", "error-message-required-city");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.zip", "error-message-required-zip");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "workplace.positionName", "error-message-required-position-name");

		String zipCode = employee.getAddress().getZip();
		if (!zipCode.matches("[0-9]+") || zipCode.length() < 3) {
			errors.rejectValue("address.zip", "error-message-incorrect-zip");
		}

		String emailAddress = employee.getEmail();
		if (!emailAddress.matches(".+@.+\\.(.*){2,3}")) {
			errors.rejectValue("email", "error-message-incorrect-email");
		}
	}
}