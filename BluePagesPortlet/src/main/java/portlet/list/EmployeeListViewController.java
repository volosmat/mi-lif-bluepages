package portlet.list;

import bluepages.backend.entity.Employee;
import bluepages.backend.service.EmployeeService;
import bluepages.backend.service.ServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import portlet.BaseController;

import javax.portlet.*;

import static configuration.EmployeeListPortletConstants.*;

@Controller
@RequestMapping("VIEW")
public class EmployeeListViewController extends BaseController {

	@Autowired
	private EmployeeValidator employeeValidator;

	@RenderMapping
	public String renderDefault(RenderRequest request, Model model) {
		// Create employee object if not exists
		if (!model.containsAttribute(FORM_MODEL)) {
			Employee employee = new Employee();
			model.addAttribute(FORM_MODEL, employee);
		}

		this.updateSelectedEmployee(model, request.getParameter(PARAMETER_EMPLOYEE_ID));

		// Add all employees to model
		model.addAttribute(EMPLOYEE_LIST, ServiceFactory.getEmployeeService().getAll());

		return JSP_DEFAULT;
	}

	@ActionMapping(SAVE_EMPLOYEE_ACTION)
	public void saveEmployeeAction(@ModelAttribute(FORM_MODEL) Employee employee, BindingResult result, Model model) {

		employeeValidator.validate(employee,result);
		if (!result.hasErrors()) {
			EmployeeService employeeService = ServiceFactory.getEmployeeService();
			employeeService.saveEmployee(employee);
			model.addAttribute("successMessage", "Employee record was saved successfully.");
		}
	}

	/**
	 * When employee id is set load employee from db and store in model
	 * @param model
	 * @param idParameter
	 */
	private void updateSelectedEmployee(Model model, String idParameter) {
		// Load employee from DB
		EmployeeService employeeService = ServiceFactory.getEmployeeService();
		Employee employee = null;

		// Check if idParameter is set
		if (idParameter != null) {
			employee = this.loadEmployee(idParameter, model);
		}

		// Store employee in model to show in edit form
		if (employee != null) {
			model.addAttribute(FORM_MODEL, employee);
		}
	}
}
