package portlet.search;

import bluepages.backend.entity.Employee;
import bluepages.backend.service.ServiceFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import portlet.BaseController;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;

import java.util.List;

import static configuration.EmployeeListPortletConstants.PARAMETER_EMPLOYEE_ID;
import static configuration.SearchPortletConstants.*;

@Controller
@RequestMapping("VIEW")
public class SearchController extends BaseController {

	@RenderMapping
	public String renderDefault(RenderRequest request, Model model) {
		String queryString = request.getParameter(QUERY_STRING_PARAMETER);
		List<Employee> result = ServiceFactory.getEmployeeService().searchEmployeeByName(queryString);
		model.addAttribute(SEARCH_RESULT_LIST, result);
		return JSP_DEFAULT;
	}

	@ActionMapping(SEARCH_ACTION)
	public void searchEmployeeAction(ActionRequest request, ActionResponse response) {
		response.setRenderParameter(QUERY_STRING_PARAMETER, request.getParameter(QUERY_STRING_PARAMETER));
	}
}
