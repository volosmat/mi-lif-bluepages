package bluepages.backend.entity;

import javax.persistence.*;

@Entity
public class Address extends BasicEntity {

	private String street;
	private String houseNumber;
	private String city;
	private String state;
	private String zip;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected int id;

	/**
	 * Used only in test data component
	 * @param street
	 * @param houseNumber
	 * @param city
	 * @param state
	 * @param zip
	 */
	public Address(String street, String houseNumber, String city, String state, String zip) {
		this.street = street;
		this.houseNumber = houseNumber;
		this.city = city;
		this.state = state;
		this.zip = zip;
	}

	public Address() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", houseNumber=" + houseNumber
				+ ", city=" + city + ", state=" + state + ", zip=" + zip + "]";
	}
}
