package bluepages.backend.entity;

import javax.persistence.*;

@Entity
public class Employee extends BasicEntity {
	@OneToOne(cascade = {CascadeType.MERGE})
	private Address address;
	@OneToOne(cascade = {CascadeType.MERGE})
	private Workplace workplace;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected int id;

	private String email;
	private String firstName;
	private String surname;

	/**
	 * Used only in test data component
	 * @param address
	 * @param firstName
	 * @param surname
	 * @param workplace
	 */
	public Employee(Address address, String firstName, String surname,
					Workplace workplace) {
		this.address = address;
		this.firstName = firstName;
		this.surname = surname;
		this.workplace = workplace;
	}

	public Employee() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Workplace getWorkplace() {
		return workplace;
	}

	public void setWorkplace(Workplace workplace) {
		this.workplace = workplace;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Employee{" +
				"address=" + address +
				", workplace=" + workplace +
				", id=" + id +
				", firstName='" + firstName + '\'' +
				", surname='" + surname + '\'' +
				'}';
	}
}
