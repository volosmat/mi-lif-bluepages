package bluepages.backend.entity;

import javax.persistence.*;

@Entity
public class Workplace extends BasicEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected int id;
	private String description;
	private String positionName;
	private String positionType;

	/**
	 * Used only in test data component
	 * @param description
	 * @param positionName
	 * @param positionType
	 */
	public Workplace(String description, String positionName,
					 String positionType) {
		this.description = description;
		this.positionName = positionName;
		this.positionType = positionType;
	}

	public Workplace() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getPositionType() {
		return positionType;
	}

	public void setPositionType(String positionType) {
		this.positionType = positionType;
	}

	@Override
	public String toString() {
		return "Workplace [description=" + description + ", positionName="
				+ positionName + ", positionType=" + positionType + "]";
	}
}
