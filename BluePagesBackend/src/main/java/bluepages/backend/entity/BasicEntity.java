package bluepages.backend.entity;

import java.io.Serializable;

public abstract class BasicEntity implements Serializable {
	public abstract int getId();

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BasicEntity that = (BasicEntity) o;

		return this.getId() == that.getId();

	}

	@Override
	public int hashCode() {
		return this.getId();
	}
}
