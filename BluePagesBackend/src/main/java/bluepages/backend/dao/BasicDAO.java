package bluepages.backend.dao;

import bluepages.backend.entity.BasicEntity;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public class BasicDAO<T extends BasicEntity> {
	@PersistenceContext
    protected EntityManager entityManager;
	protected Class<T> entityClass;

	public BasicDAO() {
		// Get type of T and use for select statement
		ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		entityClass = (Class<T>) type.getActualTypeArguments()[0];
	}

	/**
	 * Insert new data into DB (entity)
	 * @param item
	 */
    public void insert(T item) {
        entityManager.persist(item);
    }

	/**
	 * Remove entity
	 * @param item
	 */
    public void remove(T item) {
        entityManager.remove(entityManager.merge(item));
    }

	/**
	 * Update entity record in DB
	 * @param item
	 */
    public void update(T item) {
        entityManager.merge(item);
    }

	/**
	 * Find entity by entity identification
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
    public T findById(int id) throws EntityNotFoundException {
        T result = entityManager.find(entityClass, id);
        if (result == null) {
            throw new EntityNotFoundException();
        }
        return result;
    }

	/**
	 * Get all records of instanced DAO object
	 * @return
	 */
    public List<T> getAll() {
        TypedQuery<T> query = entityManager.createQuery("SELECT e FROM " + entityClass.getSimpleName() + " e", entityClass);

        return query.getResultList();
    }
}
