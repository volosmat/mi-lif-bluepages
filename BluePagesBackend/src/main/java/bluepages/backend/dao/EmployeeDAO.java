package bluepages.backend.dao;

import bluepages.backend.entity.Employee;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class EmployeeDAO extends BasicDAO<Employee> {

    public List<Employee> searchByName(String name) {
        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getSimpleName() + " e WHERE " +
                        "LOWER(e.firstName) LIKE CONCAT('%', LOWER(:firstName), '%') OR " +
                        "LOWER(e.surname) LIKE CONCAT('%', LOWER(:surname), '%')");

        q.setParameter("firstName", "%"+name+"%");
        q.setParameter("surname", "%"+name+"%");

        return q.getResultList();
    }
}
