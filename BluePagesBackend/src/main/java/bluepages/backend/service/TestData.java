package bluepages.backend.service;

import bluepages.backend.dao.AddressDAO;
import bluepages.backend.dao.EmployeeDAO;
import bluepages.backend.dao.WorkplaceDAO;
import bluepages.backend.entity.Address;
import bluepages.backend.entity.Employee;
import bluepages.backend.entity.Workplace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Component
public class TestData {
	@Autowired
	private EmployeeDAO employeeDAO;
	@Autowired
	private AddressDAO addressDAO;
	@Autowired
	private WorkplaceDAO workplaceDAO;

	@Transactional
	public void testData() {

		Address address = new Address("ulice", "409", "Praha", "cesko", "14900");
		addressDAO.insert(address);
		Workplace workplace = new Workplace("popis pozicie", "pila", "pracovník na píle");
		workplaceDAO.insert(workplace);
		Employee employee = new Employee(address, "Matus", "Volosin", workplace);
		employee.setEmail("matus@volosin.sk");
		employeeDAO.insert(employee);
		employee = new Employee(address, "Alex", "Volosin", workplace);
		employee.setEmail("alex@volosin.sk");
		employeeDAO.insert(employee);
	}
}
