package bluepages.backend.service;

import bluepages.backend.dao.AddressDAO;
import bluepages.backend.dao.EmployeeDAO;
import bluepages.backend.dao.WorkplaceDAO;
import bluepages.backend.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EmployeeService {
	@Autowired
	private EmployeeDAO employeeDAO;
	@Autowired
	private AddressDAO addressDAO;
	@Autowired
	private WorkplaceDAO workplaceDAO;

	@Transactional(readOnly=true)
    public List<Employee> getAll() {
        return employeeDAO.getAll();
    }

    @Transactional(readOnly=true)
    public Employee getEmployee(int id) {
        return employeeDAO.findById(id);
    }

    public void addEmployee(Employee employee) {
		addressDAO.insert(employee.getAddress());
		workplaceDAO.insert(employee.getWorkplace());
		employeeDAO.insert(employee);
    }

	public void updateEmployee(Employee employee) {
		addressDAO.update(employee.getAddress());
		workplaceDAO.update(employee.getWorkplace());
		employeeDAO.update(employee);
	}

	public void removeEmployee(Employee employee) {
		employeeDAO.remove(employee);
	}

	public void saveEmployee(Employee employee) {
		try {
			// Check if employee exists
			this.getEmployee(employee.getId());
			// Employee exist in db, update row
			this.updateEmployee(employee);
		} catch (EntityNotFoundException e) {
			// Record with this id is not in DB, add new row
			this.addEmployee(employee);
		}
	}

	@Transactional(readOnly=true)
	public List<Employee> searchEmployeeByName(String queryString) {
		return employeeDAO.searchByName(queryString);
	}
}
