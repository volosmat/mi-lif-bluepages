package bluepages.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Component
public class ServiceFactory {
	private static EmployeeService employeeService;

    private static TestData testData;

    @PostConstruct
    public void postConstruct() {
        testData.testData();
    }

    @Autowired
    public void setTestData(TestData testData) {
        ServiceFactory.testData = testData;
    }
	@Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        ServiceFactory.employeeService = employeeService;
    }
	
	public static EmployeeService getEmployeeService() {
        return employeeService;
    }
}
